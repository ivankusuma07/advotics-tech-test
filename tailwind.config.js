const path = require("path")
const colors = require("tailwindcss/colors")

module.exports = {
  purge: [path.resolve(__dirname, "./node_modules/litepie-datepicker/**/*.js")],
  darkMode: "class",
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        // Change with you want it
        "litepie-primary": colors.green, // color system for light mode
        "litepie-secondary": colors.green // color system for dark mode
      }
    }
  },
  variants: {
    extend: {
      cursor: ["disabled"],
      textOpacity: ["disabled"],
      textColor: ["disabled"]
    }
  },
  plugins: []
}
