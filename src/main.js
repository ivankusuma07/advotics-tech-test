import { createApp } from "vue"
import App from "./App.vue"

//tailwind
import "./index.css"

import dayjs from "dayjs" //import dayjs in your main.js
const app = createApp(App)
app.provide("dayJS", dayjs) // provide dayJS
app.config.globalProperties.$dayjs = dayjs //You can now use dayjs as $dayjs

// datepicker
import LitepieDatepicker from "litepie-datepicker"
app.use(LitepieDatepicker)

//vuestic
import { createVuestic } from "vuestic-ui" // <-
import "vuestic-ui/dist/vuestic-ui.css" // <-
app.use(createVuestic()) // <-

//apexchart
import VueApexCharts from "vue3-apexcharts"
app.use(VueApexCharts)
app.mount("#app")
